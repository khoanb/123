﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Server;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SOESupport;
using ESRI.ArcGIS.SOESupport.SOI;


namespace UserAccessSOI
{
    [ComVisible(true)]
    [Guid("eb82665b-b69a-4024-8f68-6d5803974517")]
    [ClassInterface(ClassInterfaceType.None)]
    [ServerObjectInterceptor("MapServer",//use "MapServer" if SOI extends a Map service and "ImageServer" if it extends an Image service.
        Description = "User Access retriction base on user tags [Provice:District] ",
        DisplayName = "UserAccessSOI",
        Properties = "")]
    public class UserAccessSOI : IServerObjectExtension, IRESTRequestHandler, IWebRequestHandler, IRequestHandler2, IRequestHandler
    {
        private string _soiName, _servicename;
        private IServerObjectHelper _soHelper;
        //private ServerLogger _serverLog;
        private RestSOIHelper _restSOIHelper;

        const string URL_ACCESS = "https://gis1061.esrivn.net/server/rest/services/mobi/Access/FeatureServer";
        private ITable _tblAccess;
        private ITable _tblGroupAccess;
        private long _layers = 0;
        public string[] layers;
        private IQueryFilter _query;
        private Dictionary<string, string> _lookupAccess;

        public UserAccessSOI()
        {
            _soiName = this.GetType().Name;
        }

        public void Init(IServerObjectHelper pSOH)
        {
            _soHelper = pSOH;
            //_serverLog = new ServerLogger();
            _restSOIHelper = new RestSOIHelper(pSOH);
            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".init()", 200, "Initialized " + _soiName + " SOI.");

            _servicename = pSOH.ServerObject.ConfigurationName;
        }

        protected string queryAccess(string user)
        {
            _query.WhereClause = "USERNAME='" + user + "'";
            var curs = _tblAccess.Search(_query, true);
            var fldLDEF = _tblAccess.FindField("LDEF");
            var fldLGROUPNAME = _tblAccess.FindField("GROUPNAME");
            var row = curs.NextRow();
            string access = null;


            
            //


            if (row != null)
            {
                if (Convert.ToString(row.Value[fldLDEF]).Contains("()") && row.Value[fldLGROUPNAME] != null)
                {
                    var groupname = row.Value[fldLGROUPNAME];
                    //query quyền của người dùng
                    IQueryFilter _querynguoidung = new QueryFilter();
                    _querynguoidung.WhereClause = "GROUPNAME='" + groupname + "'";
                    var cursnhomdung = _tblGroupAccess.Search(_querynguoidung, true);
                    var rownnhomdung = cursnhomdung.NextRow();
                    var ldefgroup = _tblGroupAccess.FindField("LDEF");
                    if (rownnhomdung != null)
                    {
                        for (long i = 0; i < layers.Length; i++)
                        {
                            access = (access == null) ? "\"" + long.Parse(layers[i].ToString()) + "\":\"" + rownnhomdung.Value[fldLDEF] + "\"" : access + ",\"" + long.Parse(layers[i].ToString()) + "\":\"" + rownnhomdung.Value[fldLDEF] + "\"";
                        }
                    }
                }
                else
                {
                    for (long i = 0; i < layers.Length; i++)
                    {
                        access = (access == null) ? "\"" + long.Parse(layers[i].ToString()) + "\":\"" + row.Value[fldLDEF] + "\"" : access + ",\"" + long.Parse(layers[i].ToString()) + "\":\"" + row.Value[fldLDEF] + "\"";
                    }
                }
            }

            return access;
        }

        public void Shutdown()
        {
            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".init()", 200, "Shutting down " + _soiName + " SOI.");
        }

        #region REST interceptors

        public string GetSchema()
        {
            IRESTRequestHandler restRequestHandler = _restSOIHelper.FindRequestHandlerDelegate<IRESTRequestHandler>();
            if (restRequestHandler == null)
                return null;

            return restRequestHandler.GetSchema();
        }

        public byte[] HandleRESTRequest(string Capabilities, string resourceName, string operationName,
            string operationInput, string outputFormat, string requestProperties, out string responseProperties)
        {
            responseProperties = null;
            var user = _restSOIHelper.ServerEnvironment.UserInfo.Name;
            _query = new QueryFilter();
            _lookupAccess = new Dictionary<string, string>();

            var prop = new PropertySet();
            prop.SetProperty("DATABASE", URL_ACCESS);
            var factory = (IWorkspaceFactory)new FeatureServiceWorkspaceFactory();
            var ws = (IFeatureWorkspace)factory.Open(prop, 0);
            _tblAccess = ws.OpenTable("3");
            _tblGroupAccess= ws.OpenTable("4");
            var tblConfig = ws.OpenTable("2");


            var fldLAYERS = tblConfig.FindField("LAYERS");
            _query.WhereClause = "SERVICE='" + _servicename + "'";
            var curs = tblConfig.Search(_query, true);
            var row = curs.NextRow();
            if (row != null)
            { 
                if((Convert.ToString(row.Value[fldLAYERS])).Contains(","))
                { 
                layers = Convert.ToString(row.Value[fldLAYERS]).Split(",");
                }
                else
                    layers = new string[] { "0" };
                //if  (long.TryParse(Convert.ToString(row.Value[fldLAYERS]), out _layers) )
                //{
                //   // _layers = (string)row.Value[fldLAYERS];
                //  // long
                //}
                //else
                //{
                //    _layers = 0;
                //}
                //for (int i = 0; i < layers.Length; i++)
                //{

                //}
            }

         

            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".HandleRESTRequest()", 200, _service + user);

            /*
            * Add code to manipulate REST requests here
            */

            // Find the correct delegate to forward the request too
            IRESTRequestHandler restRequestHandler = _restSOIHelper.FindRequestHandlerDelegate<IRESTRequestHandler>();
            if (restRequestHandler == null)
                return null;

            if (operationName.Equals("export") || operationName.Equals("identify") || operationName.Equals("find") || operationName.Equals("query"))
            {
                if (user != null)
                {
                    string access = null;
                    if (!_lookupAccess.TryGetValue(user, out access))
                    {
                        access = queryAccess(user);
                        _lookupAccess.Add(user, access);
                    }
                    var js = new JsonObject(operationInput);
                    //if (_layers != 0)
                    //{
                        if (access != null)
                            operationInput = String.Concat("{\"layerDefs\":{" + access + "},", operationInput.Remove(0, 1));
                    //}
                    //else
                    //{
                    //    if (access != null)
                    //        operationInput = String.Concat("{\"where\":\"" + access + "\",", operationInput.Remove(0, 1));
                    //}
                       
                }
            }
            var resp = restRequestHandler.HandleRESTRequest(
                    Capabilities, resourceName, operationName, operationInput,
                    outputFormat, requestProperties, out responseProperties);

            return resp;
        }

        #endregion

        #region SOAP interceptors

        public byte[] HandleStringWebRequest(esriHttpMethod httpMethod, string requestURL,
            string queryString, string Capabilities, string requestData,
            out string responseContentType, out esriWebResponseDataType respDataType)
        {
            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".HandleStringWebRequest()", 200, "Request received in Sample Object Interceptor for HandleStringWebRequest");

            /*
             * Add code to manipulate requests here
             */

            IWebRequestHandler webRequestHandler = _restSOIHelper.FindRequestHandlerDelegate<IWebRequestHandler>();
            if (webRequestHandler != null)
            {
                return webRequestHandler.HandleStringWebRequest(
                        httpMethod, requestURL, queryString, Capabilities, requestData, out responseContentType, out respDataType);
            }

            responseContentType = null;
            respDataType = esriWebResponseDataType.esriWRDTPayload;
            //Insert error response here.
            return null;
        }

        public byte[] HandleBinaryRequest(ref byte[] request)
        {
            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".HandleBinaryRequest()", 200, "Request received in Sample Object Interceptor for HandleBinaryRequest");

            /*
             * Add code to manipulate requests here
             */

            IRequestHandler requestHandler = _restSOIHelper.FindRequestHandlerDelegate<IRequestHandler>();
            if (requestHandler != null)
            {
                return requestHandler.HandleBinaryRequest(request);
            }

            //Insert error response here.
            return null;
        }

        public byte[] HandleBinaryRequest2(string Capabilities, ref byte[] request)
        {
            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".HandleBinaryRequest2()", 200, "Request received in Sample Object Interceptor for HandleBinaryRequest2");

            /*
             * Add code to manipulate requests here
             */

            IRequestHandler2 requestHandler = _restSOIHelper.FindRequestHandlerDelegate<IRequestHandler2>();
            if (requestHandler != null)
            {
                return requestHandler.HandleBinaryRequest2(Capabilities, request);
            }

            //Insert error response here.
            return null;
        }

        public string HandleStringRequest(string Capabilities, string request)
        {
            //_serverLog.LogMessage(ServerLogger.msgType.infoStandard, _soiName + ".HandleStringRequest()", 200, "Request received in Sample Object Interceptor for HandleStringRequest");

            /*
             * Add code to manipulate requests here
             */

            IRequestHandler requestHandler = _restSOIHelper.FindRequestHandlerDelegate<IRequestHandler>();
            if (requestHandler != null)
            {
                return requestHandler.HandleStringRequest(Capabilities, request);
            }

            //Insert error response here.
            return null;
        }

        #endregion

    }
}
