﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            IWorkspaceFactory2 factory = (IWorkspaceFactory2)(new FeatureServiceWorkspaceFactoryClass());

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var input= "{\"size\":\"1898,887\",\"bbox\":\"544593.8197137807,1773617.2653530631,546994.6890192379,1774739.647053063\",\"format\":\"png32\",\"layers\":\"show:0,1,2\",\"bboxSR\":{\"wkt\":\"PROJCS[\\\"VN2000\\\",GEOGCS[\\\"GCS_VN_2000\\\",DATUM[\\\"D_Vietnam_2000\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",107.75],PARAMETER[\\\"Scale_Factor\\\",0.9999],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0]]\"},\"dpi\":150,\"transparent\":true,\"imageSR\":{\"wkt\":\"PROJCS[\\\"VN2000\\\",GEOGCS[\\\"GCS_VN_2000\\\",DATUM[\\\"D_Vietnam_2000\\\",SPHEROID[\\\"WGS_1984\\\",6378137.0,298.257223563]],PRIMEM[\\\"Greenwich\\\",0.0],UNIT[\\\"Degree\\\",0.0174532925199433]],PROJECTION[\\\"Transverse_Mercator\\\"],PARAMETER[\\\"False_Easting\\\",500000.0],PARAMETER[\\\"False_Northing\\\",0.0],PARAMETER[\\\"Central_Meridian\\\",107.75],PARAMETER[\\\"Scale_Factor\\\",0.9999],PARAMETER[\\\"Latitude_Of_Origin\\\",0.0],UNIT[\\\"Meter\\\",1.0]]\"}}";
            textBox1.Text = String.Concat("{\"layerDefs\":{\"2\":\"PhanKhu='A1-1'\"},", input.Remove(0,1));
        }
    }
}
